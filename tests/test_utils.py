import typing
import unittest

from d3m_metadata import types, utils
from d3m_metadata.container import list


class TestUtils(unittest.TestCase):
    def test_get_type_arguments(self):
        A = typing.TypeVar('A')
        B = typing.TypeVar('B')
        C = typing.TypeVar('C')

        class Base(typing.Generic[A, B]):
            pass

        class Foo(Base[A, None]):
            pass

        class Bar(Foo[A], typing.Generic[A, C]):
            pass

        class Baz(Bar[float, int]):
            pass

        self.assertEqual(utils.get_type_arguments(Bar), {
            A: typing.Any,
            B: type(None),
            C: typing.Any,
        })
        self.assertEqual(utils.get_type_arguments(Baz), {
            A: float,
            B: type(None),
            C: int,
        })

        self.assertEqual(utils.get_type_arguments(Base), {
            A: typing.Any,
            B: typing.Any,
        })

        self.assertEqual(utils.get_type_arguments(Base[float, int]), {
            A: float,
            B: int,
        })

        self.assertEqual(utils.get_type_arguments(Foo), {
            A: typing.Any,
            B: type(None),
        })

        self.assertEqual(utils.get_type_arguments(Foo[float]), {
            A: float,
            B: type(None),
        })

    def test_issubclass(self):
        self.assertTrue(utils.is_subclass(list.List[float], types.Container))
        self.assertTrue(utils.is_subclass(list.List[float], list.List))
        self.assertTrue(utils.is_subclass(list.List[float], list.List[typing.Any]))
        self.assertTrue(utils.is_subclass(list.List[float], list.List[float]))
        self.assertFalse(utils.is_subclass(list.List[float], list.List[str]))

        T1 = typing.TypeVar('T1', bound=list.List)
        self.assertTrue(utils.is_subclass(list.List[float], T1))

        T2 = typing.TypeVar('T2', bound=list.List[float])
        self.assertTrue(utils.is_subclass(list.List[float], T2))

        T3 = typing.TypeVar('T3', bound=list.List[str])
        self.assertFalse(utils.is_subclass(list.List[float], T3))


if __name__ == '__main__':
    unittest.main()
